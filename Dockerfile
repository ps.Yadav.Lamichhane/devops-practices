FROM openjdk:8-jdk-alpine3.9

ARG FOO

ENV BAR=$FOO

MAINTAINER "Yadav Lamichhane <omegazyadav1@gmail.com>"

RUN addgroup -S omega && adduser -S omega -G omega

RUN chown -R omega:omega /home

COPY target/assignment-$FOO.jar entrypoint.sh /home/

WORKDIR /home

USER omega

EXPOSE 8090

ENTRYPOINT ["./entrypoint.sh"]
